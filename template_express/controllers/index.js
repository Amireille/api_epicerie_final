module.exports = {
  user: require('./user'),
  article: require('./article'),
  article_inventory: require('./article_inventory')
};
