module.exports = {
  fetchAll() {
    return connection.execute('SELECT * FROM users');
  },
  fetchOne(ID) {
    return connection.execute('SELECT * FROM users WHERE userId= ? ', [ID]);
  },
  delete(ID) {
    return connection.execute('DELETE FROM users WHERE userId = ?', [ID]);
  },

  add(req) {
    return connection.execute(
      'INSERT INTO users (last_name,first_name, budget) VALUES(?,?,?)',
      req
    );
  },
  modify(id, body) {
    console.log(body);

    return connection.execute(
      'UPDATE users SET last_name = ? , first_name = ? , budget = ? WHERE userId = ?',
      [body.last_name, body.first_name, body.budget, id]
    );
  }
};
