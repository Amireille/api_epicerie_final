module.exports = {
  fetchAll() {
    return connection.execute(
      'SELECT articles.articleId,quantity, name, price FROM articles,article_inventory WHERE articles.articleId=article_inventory.articleId'
    );
  },
  fetchOne(ID) {
    //TODO
  },
  delete(ID) {
    return connection.execute(
      'DELETE FROM article_inventory WHERE article_inventory.articleId = ?',
      [ID]
    );
  },
  add(req) {
    return connection.execute(
      'INSERT INTO article_inventory (quantity, articleId, inventoryId) VALUES (?,?,?)',
      req
    );
  },
  modify(id, body) {
    //TODO
  }
};
