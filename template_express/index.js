(async () => {
  const express = require('express');
  const app = express();
  const mysql = require('mysql2/promise');
  const config = require('./config.json');
  const methodOverride = require('method-override');
  const cors = require('cors');

  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
  app.use(methodOverride('_method'));

  try {
    global.connection = await mysql.createConnection(config);
    console.log('connection to database is a success');
  } catch (err) {
    console.log(err);
  }

  app.set('views', './views');
  app.set('view engine', 'pug');

  const user = require('./controllers/index').user;
  const article = require('./controllers/index').article;
  const article_inventory = require('./controllers/index').article_inventory;

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use('/users', user);
  app.use('/articles', article);
  app.use('/articles_inventory', article_inventory);

  app.get('/', (req, res) => {
    res.render('adminLayout');
  });

  app.listen(3000);
  console.log('Listening ...');
})();
